package com.rak.tinyhh;

import com.rak.tinyhh.R;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class SearchDialog extends DialogFragment {
	Button btnSearch;
	EditText etSearchText;
	Spinner spCity;

	String[] areas = { "1002", "1", "2", "115", "160", "159", "1007", "2492" };

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().setTitle(R.string.searchTitle);
		View rootView = inflater.inflate(R.layout.fragment_search_dialog, container, false);
		btnSearch = (Button) rootView.findViewById(R.id.btnSearch);
		etSearchText = (EditText) rootView.findViewById(R.id.etSearchText);

		spCity = (Spinner) rootView.findViewById(R.id.spCity);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
		        R.array.cities, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spCity.setAdapter(adapter);
		
		btnSearch.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SearchDialogListener activity = (SearchDialogListener) getActivity();
				String url = Uri.parse("https://api.hh.ru/vacancies").buildUpon()
						.appendQueryParameter("text", etSearchText.getText().toString())
						.appendQueryParameter("area", areas[spCity.getSelectedItemPosition()])
						.build().toString();
				
				activity.onReturnSearchDialog(url);
				dismiss();
			}
		});
		return rootView;
	}

	public interface SearchDialogListener {
		void onReturnSearchDialog(String inputText);
	}

}
