package com.rak.tinyhh;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import com.rak.tinyhh.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class VacancyAdapter extends BaseAdapter {

	private ArrayList<Vacancy> vacancies;
	private Context context;

	public VacancyAdapter(ArrayList<Vacancy> vacancies, Context context) {
		this.vacancies = vacancies;
		this.context = context;
	}

	@Override
	public int getCount() {
		return vacancies.size();
	}

	@Override
	public Object getItem(int position) {
		return vacancies.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.vacancy_list_item, parent, false);
			TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
			TextView tvData = (TextView) convertView.findViewById(R.id.tvDate);
			TextView tvEmplName = (TextView) convertView.findViewById(R.id.tvEmployerName);
			TextView tvSalary = (TextView) convertView.findViewById(R.id.tvSalary);
			ImageView image = (ImageView) convertView.findViewById(R.id.image_item);
			ViewHolder vh = new ViewHolder(tvName, tvEmplName, tvData, image, tvSalary);
			convertView.setTag(vh);
		}

		ViewHolder vh = (ViewHolder) convertView.getTag();
		vh.name.setText(vacancies.get(position).getName());
		vh.employerName.setText(vacancies.get(position).getEmployerName());
		vh.date.setText(vacancies.get(position).getDate());

		// load image
		vh.imageURL = vacancies.get(position).getEmployerLogo();
		new DownloadAsyncTask().execute(vh);

		vh.salary.setText(vacancies.get(position).getSalary());
		if (vacancies.get(position).getSalary().equals(""))
			vh.salary.setVisibility(View.GONE);
		return convertView;
	}

	private class ViewHolder {
		public final TextView name;
		public final TextView date;
		public final TextView employerName;
		public final TextView salary;
		public final ImageView image;
		public String imageURL;
		public Bitmap bitmap;

		public ViewHolder(TextView name, TextView employerName, TextView date, ImageView image,
				TextView salary) {
			this.name = name;
			this.date = date;
			this.employerName = employerName;
			this.image = image;
			this.salary = salary;
			this.imageURL = "";
		}

	}

	private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {

		@Override
		protected ViewHolder doInBackground(ViewHolder... params) {
			// load image directly
			ViewHolder viewHolder = params[0];
			try {
				URL imageURL = new URL(viewHolder.imageURL);
				viewHolder.bitmap = BitmapFactory.decodeStream(imageURL.openStream());
			} catch (IOException e) {
				Log.e("error", "Downloading Image Failed");
				viewHolder.bitmap = null;
			}

			return viewHolder;
		}

		@Override
		protected void onPostExecute(ViewHolder result) {
			if (result.bitmap == null) {
				result.image.setImageResource(R.drawable.ic_contact_picture);
			} else {
				result.image.setImageBitmap(result.bitmap);
			}
		}
	}

}
