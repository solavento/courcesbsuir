package com.rak.tinyhh;

import com.rak.tinyhh.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class Detail extends Fragment {
		
	public Detail() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
		TextView tvName = (TextView) rootView.findViewById(R.id.tvName);
		TextView tvDate = (TextView) rootView.findViewById(R.id.tvDate);
		TextView tvEmployerName = (TextView) rootView.findViewById(R.id.tvEmployerName);
		TextView tvLink = (TextView) rootView.findViewById(R.id.tvLink);
		TextView tvSalary = (TextView) rootView.findViewById(R.id.tvSalary);
		WebView wvDescription = (WebView) rootView.findViewById(R.id.webView);
		Vacancy vacancy;
		if (getArguments() != null) {
			vacancy = getArguments().getParcelable("vacancy");
			tvName.setText(vacancy.getName());
			tvDate.setText(vacancy.getDate());
			tvEmployerName.setText(vacancy.getEmployerName());
			tvLink.setText(vacancy.getAltUrl());
			tvLink.setMovementMethod(LinkMovementMethod.getInstance());
			if (vacancy.getSalary().equals(""))
				tvSalary.setText("�/� �� �������");
			else
				tvSalary.setText(vacancy.getSalary());

			wvDescription.setWebViewClient(new WebViewClient() {
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					view.loadUrl(url);
					return true;
				}
			});
			
			wvDescription.loadUrl("https://m.hh.ru/vacancy/"+vacancy.getId());
		}
		return rootView;
	}
	
}
