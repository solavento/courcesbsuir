package com.rak.tinyhh;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class Vacancy implements Parcelable {
	private String mId;
	private String mName;
	private String mDate;
	private String mEmployerName;
	private String mEmployerLogo;
	private String mSalary;
	private String mUrl;
	private String mAltUrl;

	public String toString() {
		return mName;
	}

	public Vacancy(String id, String name, String date, String employerName, String employerLogo,
			String salary, String url, String altUrl) {
		this.setId(id);
		this.setName(name);
		this.setDate(date);
		this.setEmployerName(employerName);
		this.setEmployerLogo(employerLogo);
		this.setSalary(salary);
		this.setUrl(url);
		this.setAltUrl(altUrl);
	}

	private Vacancy(Parcel dest) {
		this.setId(dest.readString());
		this.setName(dest.readString());
		this.setDate(dest.readString());
		this.setEmployerName(dest.readString());
		this.setEmployerLogo(dest.readString());
		this.setSalary(dest.readString());
		this.setUrl(dest.readString());
		this.setAltUrl(dest.readString());
	}

	public static Vacancy fromJson(final JSONObject jVacancy) {
		try {
			String id = jVacancy.getString("id");
			String name = jVacancy.getString("name");
			String date = convertDate(jVacancy.getString("published_at"));
			JSONObject jEmployer = jVacancy.getJSONObject("employer");
			String employerName = jEmployer.getString("name");
			String employerLogo = "";
			if (!jEmployer.isNull("logo_urls")) {
				JSONObject jEmployerLogo = jEmployer.getJSONObject("logo_urls");
				employerLogo = jEmployerLogo.getString("90");
			}
			String url = jVacancy.getString("url");
			String altUrl = jVacancy.getString("alternate_url");
			return new Vacancy(id, name, date, employerName, employerLogo, salary(jVacancy), url, altUrl);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String convertDate(String date) {
		DateFormat hhDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
		DateFormat myDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
		try {
			Date pubDate = hhDateFormat.parse(date);
			date = myDateFormat.format(pubDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	private static String salary(JSONObject jVacancy) {
		String salary = "";
		if (!jVacancy.isNull("salary")) {
			JSONObject jSalary;
			try {
				jSalary = jVacancy.getJSONObject("salary");
				if (!jSalary.isNull("from"))
					salary = "�� " + jSalary.getInt("from");
				if (!jSalary.isNull("to"))
					salary = salary + " �� " + jSalary.getInt("to");
				if (!jSalary.isNull("currency"))
					salary = salary + " " + jSalary.getString("currency");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return salary;
	}

	public static ArrayList<Vacancy> fromJson(final JSONArray array) {
		final ArrayList<Vacancy> vacancies = new ArrayList<Vacancy>();

		for (int index = 0; index < array.length(); ++index) {
			try {
				final Vacancy vacancy = fromJson(array.getJSONObject(index));
				vacancies.add(vacancy);
			} catch (final JSONException ignored) {
			}
		}
		return vacancies;
	}

	public String getId() {
		return mId;
	}

	public void setId(String Id) {
		this.mId = Id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String Name) {
		this.mName = Name;
	}

	public String getDate() {
		return mDate;
	}

	public void setDate(String Date) {
		this.mDate = Date;
	}

	public String getEmployerName() {
		return mEmployerName;
	}

	public void setEmployerName(String EmployerName) {
		this.mEmployerName = EmployerName;
	}

	public String getEmployerLogo() {
		return mEmployerLogo;
	}

	public void setEmployerLogo(String EmployerLogo) {
		this.mEmployerLogo = EmployerLogo;
	}

	public String getSalary() {
		return mSalary;
	}

	public void setSalary(String Salary) {
		this.mSalary = Salary;
	}
	
	public String getUrl() {
		return mUrl;
	}

	public void setUrl(String Url) {
		this.mUrl = Url;
	}

	public String getAltUrl() {
		return mAltUrl;
	}

	public void setAltUrl(String AltUrl) {
		this.mAltUrl = AltUrl;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mId);
		dest.writeString(mName);
		dest.writeString(mDate);
		dest.writeString(mEmployerName);
		dest.writeString(mEmployerLogo);
		dest.writeString(mSalary);
		dest.writeString(mUrl);
		dest.writeString(mAltUrl);
	}
	

	public static final Parcelable.Creator<Vacancy> CREATOR = new Parcelable.Creator<Vacancy>() {
		public Vacancy createFromParcel(Parcel in) {
	      return new Vacancy(in);
	    }

	    public Vacancy[] newArray(int size) {
	      return new Vacancy[size];
	    }
	  };
}
