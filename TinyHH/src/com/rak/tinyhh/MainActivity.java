package com.rak.tinyhh;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.rak.tinyhh.R;
import com.rak.tinyhh.SearchDialog.SearchDialogListener;

public class MainActivity extends ActionBarActivity implements SearchDialogListener {

	public static final String TAG = "Tiny hh";
	public static DialogFragment SearchDialog;
	public static FragmentManager mFragmentManager;
	public static Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment())
					.commit();
		}
		mContext = this;
		SearchDialog = new SearchDialog();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		mFragmentManager = getSupportFragmentManager();
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_search) {
			SearchDialog.show(getSupportFragmentManager(), "SearchDialog");
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends ListFragment {
		public static ListFragment mListFragment;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			mListFragment = this;
			return rootView;
		}

		public void onListItemClick(ListView vacanciesList, View v, int position, long id) {
			super.onListItemClick(vacanciesList, v, position, id);
			android.support.v4.app.FragmentTransaction mFTrans = mFragmentManager.beginTransaction();
			Bundle bundle = new Bundle();
			bundle.putParcelable("vacancy", (Vacancy) vacanciesList.getItemAtPosition(position));
			Detail detailFragment = new Detail();
			detailFragment.setArguments(bundle);
			mFTrans.replace(R.id.container, detailFragment);
			mFTrans.addToBackStack(null);
			mFTrans.commit();
		}
	}

	@Override
	public void onReturnSearchDialog(String inputText) {
		new ReceiveDataTask().execute(inputText);
		android.support.v4.app.FragmentTransaction mFTrans = mFragmentManager.beginTransaction();
		mFTrans.replace(R.id.container, new PlaceholderFragment());
		mFTrans.addToBackStack(null);
		mFTrans.commit();
	}

	private class ReceiveDataTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... url) {
			String result;
			try {
				result = new ReceiveData().getUrl(url[0]);
			} catch (IOException ioe) {
				Log.e(TAG, "Failed to fetch URL: ", ioe);
				result = "Connection error";
			}

			return result;
		}

		@Override
		protected void onPostExecute(String jsonString) {
			setListView(jsonString);
		}
	}
	
	public void setListView(String jsonString){
		JSONObject jObject;
		JSONArray jVacancies;
		try {
			jObject = new JSONObject(jsonString);
			jVacancies = jObject.getJSONArray("items");
			ArrayList<Vacancy> vacancies = Vacancy.fromJson(jVacancies);
			BaseAdapter vacancyAdapter = new VacancyAdapter(vacancies, getApplicationContext());
			PlaceholderFragment.mListFragment.setListAdapter(vacancyAdapter);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	

	public class ReceiveData {
		byte[] getUrlBytes(String urlSpec) throws IOException {
			URL url = new URL(urlSpec);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			try {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				InputStream in = connection.getInputStream();
				if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
					return null;
				}
				int bytesRead = 0;
				byte[] buffer = new byte[1024];
				while ((bytesRead = in.read(buffer)) > 0) {
					out.write(buffer, 0, bytesRead);
				}
				out.close();
				return out.toByteArray();
			} finally {
				connection.disconnect();
			}
		}

		public String getUrl(String urlSpec) throws IOException {
			return new String(getUrlBytes(urlSpec));
		}
	}
}
